package com.classpathio.orders.event;

public enum OrderStatus {
	ORDER_ACCEPTED,
	ORDER_FULLFILLED,
	ORDER_CANCELLED
}

