# step by step instructions to create a Docker image
# Theses instructions will be interpreted by the docker client
# to generate the docker image

# Set of commands to create a docker image
# the instructions to convert our application to a docker image
# Base image
# docker commands
#start with the Base image
FROM openjdk:11-jdk-slim as builder

WORKDIR /app

#copies file/directory from src location to destination location
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .

#creates a container
#download all the dependencies using the dependency maven plugin and the fo-offline goal
RUN chmod +x ./mvnw && ./mvnw -B dependency:go-offline
#builds an image and disposes the container

COPY src src
RUN ./mvnw package -DskipTests

RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

#------ completes the build stage --------------------#
# Download a bran new jre image
FROM openjdk:11.0.13-jre-slim-buster as stage

#argument
ARG DEPENDENCY=/app/target/dependency


# Copy the dependency application file from builder stage artifact
COPY --from=builder ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=builder ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=builder ${DEPENDENCY}/BOOT-INF/classes /app

EXPOSE 8222

ENTRYPOINT ["java", "-cp", "app:app/lib/*", "com.classpathio.orders.OrdersApiApplication"]






